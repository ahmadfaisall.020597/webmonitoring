import axios from "axios";
import { useMemo } from "react";
import { useNavigate } from "react-router";

export default function LoginPage() {
  const navigateTo = useNavigate();

  function login() {
    const form = new FormData();
    form.append('email', document.getElementById('email').value)
    form.append('password', document.getElementById('password').value)

    axios.get('/sanctum/csrf-cookie').then(() => {
      axios.post('login', form).then((response) => console.log(response));
    })
  }

  return (
    <div className="w-screen h-screen flex flex-row">
      <div className="h-screen w-[60%] bg-white flex flex-col items-center justify-center" >
        <div className="flex flex-col items-center p-20 border-2 border-slate-200">
          <h1 className="font-poppins font-bold text-5xl text-black">Welcome Back</h1>
          <div className="h-3" />
          <p className="text-lg font-poppins text-black">Don't have an account, yet? {' '}
            <span className="text-blue-400 underline font-poppins text-lg cursor-pointer" onClick={() => navigateTo('/register')}>Sign up</span>
          </p>
          <div className="h-14" />

          <form className="flex flex-col w-full">
            <label htmlFor="email">
              <input className="p-3 w-full bg-slate-100" id="email" name="email" type="email" placeholder="name@provider.com" />
            </label>
            <div className="h-6" />
            <label htmlFor="password">
              <input className="p-3 w-full bg-slate-100" id="password" name="password" type="password" placeholder="••••••••" />
            </label>

            <div className="h-6" />
            <button className="w-full bg-blue-600 text-white font-poppins text-lg rounded-xl px-2 py-2" type="button" onClick={login}>
              Sign in
            </button>
          </form>

        </div>
      </div>
      <div className="h-screen w-[40%] bg-red-200 bg-center bg-cover bg-no-repeat"
        style={{ backgroundImage: `url(https://images.pexels.com/photos/2322425/pexels-photo-2322425.png)` }} />
    </div>
  )
}
