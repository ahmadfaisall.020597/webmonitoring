import axios from 'axios'
import React, { useEffect } from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import RootPage from './features/root/RootPage'
import RegisterPage from './features/register/RegisterPage'
import LoginPage from './features/login/LoginPage'
import ChartPage from './features/chart/ChartPage'
import TablePage from './features/table/TablePage'

axios.defaults.withCredentials = true;
axios.defaults.baseURL = 'http://localhost';

function Logout() {
  useEffect(() => {
    axios.get('/sanctum/csrf-cookie').then(() => {
      axios.post('/logout')
    })
  }, [])
  return <div />
}

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<RootPage />} />
        <Route path="/logout" element={<Logout />} />
        <Route path="/login" element={<LoginPage />} />
        <Route path="/register" element={<RegisterPage />} />
        <Route path="/chart" element={<ChartPage />} />
        <Route path="/table" element={<TablePage />} />
      </Routes>
    </BrowserRouter>
  </React.StrictMode>
)
