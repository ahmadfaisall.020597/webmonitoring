import ChartSquareBar from "../../icons/ChartSquareBar"
import User from "../../icons/user"
import Table from "../../icons/Table"
import Report from "../../icons/Report"
import { useNavigate } from 'react-router';
import ChartPage from "../chart/ChartPage";

function Dashboard() {
    const navigateTO = useNavigate();
    return (
        <div className="w-screen h-screen flex flex-col">
            <div className=' flex flex-row justify-between w-full h-12 bg-sky-900 flex flex-row' >
                <p className="text-white font-anton text-3xl pl-3 pt-2">Dashboard</p>
                <p className="pt-1 pr-2 hover:bg-sky-700"><User /></p>
            </div>
            <div className="h-full flex flex-row">
                <div className="bg-sky-900 w-full w-48 flex flex-col w- h-full pt-3">
                    <div className="flex flex-row space-x-2 hover:bg-sky-700 pl-1">
                        <ChartSquareBar />
                        <span className="text-white text-xl cursor-pointer" onClick={() => navigateTO('/chart')}>Chart</span>
                    </div>
                    <div className="flex flex-row space-x-2 hover:bg-sky-700 pl-1 ">
                        <Table />
                        <p className="text-white text-xl">Table</p>
                    </div>
                    <div className="flex flex-row space-x-2 hover:bg-sky-700 pl-1">
                        <Report />
                        <p className="text-white text-xl">Report</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Dashboard